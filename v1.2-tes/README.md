This folder contains schemas from version 1.2 of the CWL language, extended for the v1.2-tes version, used
internally.

The original v1.2 and relevant information can be found on the CWL repository: 
https://github.com/common-workflow-language/cwltool/tree/main/cwltool/schemas

